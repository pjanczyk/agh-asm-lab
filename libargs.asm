_DATA segment

  ; VARIABLES:

  ; arg_count : byte
  ;   Number of arguments
  arg_count db 0

  ; args : byte[128]
  ;   Array of arguments encoded as null-terminated
  ;   strings, array is ended with one more null character
  args db 128 dup (?)

_DATA ends


_CODE segment

  ;----------------------------------------------------------------
  ; Returns the segment of PSP.
  ;
  ; returns:
  ;   AX -- segment of PSP
  ;----------------------------------------------------------------
  get_psp_seg proc uses bx
      ; Int 21/AH=62h -- GET CURRENT PSP ADDRESS
      ; Return:
      ;   BX = segment of PSP for current process
      mov ah, 62h
      int 21h
      mov ax, bx
      ret
  get_psp_seg endp


  ;----------------------------------------------------------------
  ; Determines whether the given character is white space,
  ; i.e. whether it is '\t', '\n', '\v', '\f', '\r' or ' '.
  ;
  ; arguments:
  ;   byte -- character to classify
  ;
  ; returns:
  ;   AL -- 1 if the character is white space, 0 otherwise
  ;----------------------------------------------------------------
  char_is_whitespace proc char:byte
      mov al, char
      cmp al, 09h ; horizontal tab
      je ret_true
      cmp al, 0Ah ; line feed (new line)
      je ret_true
      cmp al, 0Bh ; vertical tab
      je ret_true
      cmp al, 0Ch ; form feed
      je ret_true
      cmp al, 0Dh ; carriage return
      je ret_true
      cmp al, 20h ; space
      je ret_true

      xor al, al ; AL == 0 (false)
      ret

    ret_true:
      mov al, 1 ; AL == 1 (true)
      ret
  char_is_whitespace endp


  ;----------------------------------------------------------------
  ; Parses command-line tail from DOS's PSP segment.
  ;
  ; Sets global variable `arg_count` to the number of arguments.
  ; Fills global array `args` with arguments encoded as
  ; null-terminated strings, array is ended with one more null
  ; character.
  ;----------------------------------------------------------------
  parse_args proc uses ds es ax bx cx dx si di
      ; DS -- segment of PSP
      ; DS:BX -- iterator over PSP's command-line tail
      ; DS:DX -- end of PSP's command-line tail
      ; DS:SI -- start of current argument    
      ; ES -- .data segment
      ; ES:DI -- points to the global array `args`
      ; AX, CX -- temporary values

      cld ; clear direction flag (used later by movsb instruction)

      mov ax, _DATA
      mov es, ax ; ES == .data segment
      mov di, offset args ; ES:DI == `args`

      call get_psp_seg
      mov ds, ax ; DS == segment of PSP

      mov bx, 81h ; DS:BX == PSP:81h -- start of command-line tail

      xor dh, dh
      mov dl, byte ptr ds:[80h] ; DX == [PSP:80h] -- length of command-line tail
      add dx, bx ; DS:DX -- end of command-line tail

      ; BX -- iterator over command-line tail
      ; it is increased until it reaches end (DX)

    process_whitespaces:
      cmp bx, dx
      je finish ; reached end, nothing more to do
      mov al, byte ptr [bx] ; AL -- current character
      push ax
      call char_is_whitespace ; AL == 0 or 1
      cmp al, 0
      jz begin_arg ; AL == 0, current character is non-whitespace
      inc bx ; move iterator one character forward
      jmp process_whitespaces

    begin_arg:
      mov si, bx ; DS:SI -- start of current argument (current non-whitespace fragment)

    process_arg:
      cmp bx, dx
      je store_arg ; reached end, save current argument
      mov al, byte ptr [bx] ; AL -- current character
      push ax
      call char_is_whitespace ; AL == 0 or 1
      cmp al, 0
      jnz store_arg ; AL == 1, current character is whitespace 
      inc bx ; move iterator one character forward
      jmp process_arg

    store_arg:
      mov cx, bx
      sub cx, si ; CX == BX - SI -- length of current argument

      rep movsb ; copy CX bytes from DS:SI to ES:DI,
                ; i.e. copy current argument to the end of global array `args`
      mov al, 0
      stosb ; terminate current argument with null character

      inc byte ptr es:[arg_count] ; update global `arg_count`
      jmp process_whitespaces

    finish:
      mov al, 0
      stosb ; terminate array `args` with null character
      ret
  parse_args endp


  ;----------------------------------------------------------------
  ; Returns a pointer to an argument of a given number.
  ;
  ; arguments:
  ;   byte -- the number of the argument
  ;
  ; returns:
  ;   AX -- pointer to the argument represented as
  ;         a null-terminated string
  ;----------------------------------------------------------------
  get_arg proc uses bx cx, arg_no:byte

      mov cl, arg_no
      mov bx, offset args

    outer_loop_begin:
      cmp cl, 0
      je outer_loop_end

    inner_loop_begin:
      cmp byte ptr ds:[bx], 0
      je inner_loop_end
      inc bx
      jmp inner_loop_begin
    inner_loop_end:

      inc bx
      dec cl
      jnz outer_loop_begin

    outer_loop_end:

      mov ax, bx
      ret

  get_arg endp


_CODE ends
