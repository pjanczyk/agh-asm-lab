;----------------------------------------------------------------
; (C) 2017  Piotr Janczyk
;
; Implementation of OpenSSH fingerprint visualization algorithm.
; MASM, 80186 processor, 16-bit mode, DOS
;----------------------------------------------------------------

.186
.model small, stdcall

include libbase.asm
include libargs.asm


_DATA segment

  ; VARIABLES:

  ; inverted_mode : byte 
  ;   Boolean flag which indicates whether special inverted mode is enabled.
  ;   By default disabled.
  inverted_mode db 0
  
  ; hash : byte[16]
  ;   128-bit hash to be visualized.
  hash db 16 dup (?)
  
  ; board : byte[153]
  ;   2D array of size 17x9, represents board which will be printed to user.
  board db 153 dup (0)

  ; CONSTANTS:

  ascii_map db " .o+=*BOX@%&#/^SE" ; 17 characters
  str_invalid_args db "Invalid arguments.", 13, 10, "Usage: asciiart.exe [FLAG] HASH", 13, 10, 0
  str_border_top db 201, 17 dup (205), 187, 13, 10, 0 ; "╔═════════════════╗\r\n"
  str_border_left db 186, 0 ; "║"
  str_border_right db 186, 13, 10, 0 ; "║\r\n"
  str_border_bottom db 200, 17 dup (205), 188, 13, 10, 0 ; "╚═════════════════╝\r\n"

_DATA ends


_CODE segment

  ;----------------------------------------------------------------
  ; Parses inverted mode flag. Allowed values are '0' and '1'.
  ; If input string is in correct format stores decoded value
  ; to global variable `inverted_mode`
  ; 
  ; arguments:
  ;   byte ptr -- pointer to null-terminated input string
  ;
  ; returns:
  ;   AX : byte ptr -- on success: pointer to the last character of the input string (null character)
  ;                    on error: 0
  ;----------------------------------------------------------------
  parse_flag proc uses bx, input:ptr byte
      mov bx, input
      mov al, byte ptr [bx] ; AL -- first character of input

      cmp al, '0'
      je expect_null ; AL == '0' -- inverted_mode is 0 by default, do nothing
      cmp al, '1'
      jne invalid_flag ; AL != '0' && AL != '1' -- invalid

      ; AL == '1'
      mov byte ptr ds:[inverted_mode], 1 ; update flag

    expect_null:
      inc bx
      cmp byte ptr [bx], 0
      jne invalid_flag ; next character is not '\0'
      mov ax, bx
      ret

    invalid_flag:
      xor ax, ax ; return 0
      ret
  parse_flag endp


  ;----------------------------------------------------------------
  ; Parses 128-bit hash represented by 32 hexadecimal characters.
  ; Checks if input string is in correct format, i.e. /^[0-9a-f]{32}$/
  ; and, if so, stores decoded hash to global array `hash`.
  ; 
  ; arguments:
  ;   byte ptr -- pointer to null-terminated input string
  ;
  ; returns:
  ;   AX : byte ptr -- on success: pointer to the last character of the input string (null character)
  ;                    on error: 0
  ;----------------------------------------------------------------
  parse_hash proc uses bx si di, input:ptr byte
      ; BX -- `input`
      ; SI -- iterator over input string

      mov bx, input ; BX == input
      xor si, si ; SI == 0

    loop_start:
      cmp si, 32 ; SI > 32 -- either reached end of argument or argument is too long (invalid)
      je finish
      mov al, byte ptr [bx+si]
      cmp al, 0 
      je finish ; AL == '\0' -- reached end of argument

    check_digit:
      cmp al, '0'
      jb invalid ; AL < '0' -- invalid character
      cmp al, '9'
      ja check_letter ; AL > '9' -- not a digit, may be a letter
      ; AL >= '0' && AL <= '9' -- valid digit character
      sub al, '0' ; AL in range 0-9
      jmp store_nibble
      
    check_letter:
      cmp al, 'a'
      jb invalid ; AL > '9' && AL < 'a' -- invalid character
      cmp al, 'f'
      ja invalid ; AL > 'f' -- invalid character 
      ; AL > 'a' && AL <= 'f' -- valid letter character
      sub al, ('a' - 10) ; AL in range 10-15
      
    store_nibble:
      mov di, si
      inc si
      shr di, 1 ; DI = SI / 2,  CF = SI % 2,  SI++
      jc store_lower_nibble ; SI was odd -- lower nibble
      ; SI was even -- higher nibble
      
    store_higher_nibble:
      shl al, 4
      mov byte ptr ds:[hash+di], al
      jmp loop_start
      
    store_lower_nibble:
      or byte ptr ds:[hash+di], al
      jmp loop_start
    
    finish:
      cmp si, 32
      jne invalid
      mov al, byte ptr [bx+si]
      cmp al, 0
      jne invalid
      
      mov ax, bx ; AX == input
      add ax, si ; AX == input[SI] -- pointer to '\0'
      ret
      
    invalid:
      xor ax, ax ; AX == 0
      ret

  parse_hash endp


  ;----------------------------------------------------------------
  ; Fills global `board` with values 0-16 according to hash
  ; visualization algorithm.
  ; Meaning of values:
  ;   0-13 -> square was visited this number of times
  ;     14 -> square was visited at least 14 times
  ;     15 -> start position
  ;     16 -> final position
  ;----------------------------------------------------------------
  fill_board proc uses ax bx cx dx si di
      ; 153 squares -- 17 columns x 9 rows
      ; pos(x, y) := x + 17*y
      ; start position: (8, 4) -- 76 -- center of board
      ;
      ; bits at positions 0, 2, 4, 6:
      ;   0 -> move left
      ;   1 -> move right
      ; bits at positions 1, 3, 5, 7:
      ;   0 -> move up
      ;   1 -> move down

      ; SI -- iterator over `hash`, range 0-15
      ; DI -- 2D cursor position mapped to linear index, range 0-152
      ; DL -- cursor X position, range 0-16
      ; DH -- cursor Y position, range 0-8
      ; CX -- loop counter, range 0-4
      ; AX, BX -- temporary

      xor si, si ; SI == 0
      mov dl, 8 ; X == 8
      mov dh, 4 ; Y == 4

    load_next_byte:
      mov bl, byte ptr ds:[hash+si]
      mov cx, 4
      cmp byte ptr ds:[inverted_mode], 0
      je move_horizontally
      not bl ; if inverted mode is enabled negate bytes

    move_horizontally:
      ror bl, 1 ; CF == least significant bit
      jc move_right ; 1 -> move right
                    ; 0 -> move left
    move_left:
      cmp dl, 0
      je move_vertically ; X == 0, skip move
      dec dl ; X--
      jmp move_vertically
    move_right:
      cmp dl, 16
      je move_vertically ; X == 16, skip move
      inc dl ; X++

    move_vertically:
      ror bl, 1 ; CF == least significant bit
      jc move_down ; 1 -> move down
                   ; 0 -> move up
    move_up:
      cmp dh, 0
      je update_board ; Y == 0, skip move
      dec dh ; Y--
      jmp update_board
    move_down:
      cmp dh, 8
      je update_board ; Y == 8, skip move
      inc dh ; Y++

    update_board:
      mov al, dh ; AL == Y
      mov ah, 17
      mul ah ; AX == 19*Y
      add al, dl ; AX == 19*Y + X
      mov di, ax ; DI == 19*Y + X
      cmp byte ptr ds:[board+di], 14
      jge do_not_update_board ; if value at current position >= 14 don't increase it
      inc byte ptr ds:[board+di]
    do_not_update_board:
      loop move_horizontally ; 4*2=8 moves per byte

      inc si
      cmp si, 16
      jne load_next_byte ; SI < 16, procced next byte
      ; SI == 16, all bytes have been processed, break loop

      mov byte ptr ds:[board+76], 15 ; mark start position
      mov byte ptr ds:[board+di], 16 ; mark end position

      ret

  fill_board endp


  ;----------------------------------------------------------------
  ; Prints global 2D array `board`.
  ; Values at squares are mapped using `ascii_map`.
  ;----------------------------------------------------------------
  print_board proc uses ax bx cx si
      ; SI -- iterator over array `board`, range 0-152
      ; CX -- current column, range 0-16

      invoke print_cstring, addr str_border_top

      xor cx, cx ; CX == 0
      xor si, si ; SI == 0

    loop_start:
      cmp cx, 0
      jg print_square ; CX > 0
      ; CX == 0 -- first column
      invoke print_cstring, addr str_border_left

    print_square:
      xor bx, bx
      mov bl, byte ptr ds:[board+si] ; BX == board[SI]
      mov al, byte ptr ds:[ascii_map+bx] ; AL == ascii_map[board[SI]]
      print_char al ; print character that represents current square

      cmp cx, 16
      jl loop_footer ; CX < 16
      ; CX == 16 -- last column
      mov cx, (-1)
      invoke print_cstring, addr str_border_right

    loop_footer:
      inc cx
      inc si
      cmp si, 153
      jl loop_start ; SI < 153
      ; break loop if SI == 153

      invoke print_cstring, addr str_border_bottom
      ret
  print_board endp


  start proc
      mov ax, _STACK
      mov ss, ax
      mov sp, offset stack_top

      mov ax, _DATA
      mov ds, ax ; DS == data segment

      call parse_args ; fills `args` and `arg_count`

      mov ax, offset args ; AX == args

      cmp ds:[arg_count], 1
      je handle_hash_arg ; arg_count == 1

      cmp ds:[arg_count], 2
      jne invalid_args ; arg_count != 1 && arg_count != 2 -- invalid number of arguments

      ; arg_count == 2
      invoke parse_flag, ax ; if success AX points to the end of argument
                            ; if error AX == 0
      cmp ax, 0
      je invalid_args ; parse_flag returned 0 -- error
      inc ax ; AX points to the beginning of next argument

    handle_hash_arg:
      invoke parse_hash, ax ; if success AX points to the end of argument
                            ; if error AX == 0
      cmp ax, 0
      je invalid_args

      call fill_board
      call print_board

      invoke exit, 0

    invalid_args:
      invoke print_cstring, addr str_invalid_args ; print error message
      invoke exit, 1 ; exit with non-zero code

  start endp

_CODE ends


_STACK segment stack
    dw 3ffh dup(?)
  stack_top:
    dw ?
_STACK ends


end start
