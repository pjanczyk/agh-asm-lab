;----------------------------------------------------------------
; (C) 2017  Piotr Janczyk
;
; Program drawing Koch curve (Koch snowflake) using an L-system.
; DOS, 16-bit Real Mode x86, video mode 13h (320x200 pixels, 256 colors)
;----------------------------------------------------------------

.186
.model small, stdcall

include libbase.asm
include libargs.asm

SCREEN_WIDTH equ 320
SCREEN_HEIGHT equ 200
BUFFOR_SIZE equ 30000
MIN_ITERATIONS equ 0
MAX_ITERATIONS equ 6
MIN_SEGMENT_LENGTH equ 1
MAX_SEGMENT_LENGTH equ 100


_DATA segment

  l_system_buf1 db 'F++F++F', (BUFFOR_SIZE-7) dup (0)
  l_system_buf2 db BUFFOR_SIZE dup (0)

  ; phrase
  f_replacement db 'F-F++F-F', 0

  start_x dw 160
  start_y dw 195

  fpu_cw dw ?

  ; sin & cos of angles 0, 60, 120, 180, 240, 320 degrees
  sin dq 00000000000000000h, 3FEBB67AE8584CAAh, 03FEBB67AE8584CABh, 03CA1A62633145C07h, 0BFEBB67AE8584CA8h, 0BFEBB67AE8584CAEh, 0BCD469898CC51702h
  cos dq 03FF0000000000000h, 3FE0000000000001h, 0BFDFFFFFFFFFFFFCh, 0BFF0000000000000h, 0BFE0000000000004h, 03FDFFFFFFFFFFFF4h, 03FF0000000000000h

  previous_video_mode db 0

  iterations dw 0 ; number of L-system iterations
  segment_length dw 0 ; length of a single segment of curve

  str_invalid_args db "Invalid arguments.", 13, 10, "Usage: koch-curve.exe ITERATIONS LENGTH", 13, 10,
      "   ITERATIONS   0..6    number of l-system iterations", 13, 10,
      "   LENGTH       1..100  length of segments", 13, 10, 0

_DATA ends


_CODE segment

  ;----------------------------------------------------------------
  ; Reads single character from standard input (without echo)
  ;
  ; returns:
  ;   AL : byte -- the read character
  ;----------------------------------------------------------------
  read_char proc
      ; Int 21/AH=08h - DOS 1+ - CHARACTER INPUT WITHOUT ECHO
      ;   AH = 08h
      ;  Return:
      ;   AL = character read from standard input
      mov ah, 08h
      int 21h

      ; AL == read character
      ret
  read_char endp


  ;----------------------------------------------------------------
  ; Returns a current video mode.
  ;
  ; returns:
  ;   AL : byte -- the current video mode
  ;----------------------------------------------------------------
  get_video_mode proc uses bx
      ; Int 10/AH=0Fh - VIDEO - GET CURRENT VIDEO MODE
      ;   AH = 0Fh
      ;  Return:
      ;   AH = number of character columns
      ;   AL = display mode
      ;   BH = active page
      mov ah, 0Fh
      int 10h

      ; AL == display mode
      ret
  get_video_mode endp


  ;----------------------------------------------------------------
  ; Sets video mode.
  ;
  ; arguments:
  ;   mode : byte -- desired video mode
  ;----------------------------------------------------------------
  set_video_mode proc uses ax, mode:byte
      ; Int 10/AH=00h - VIDEO - SET VIDEO MODE
      ;   AH = 00h
      ;   AL = desired video mode
      ;  Return:
      ;   AL = video mode flag
      mov ah, 0
      mov al, mode
      int 10h

      ret
  set_video_mode endp


  ;----------------------------------------------------------------
  ; Sets pixel at a given coordinates.
  ; Procedure assumes that current video mode is 0x13 and sets pixel
  ; by writing to memory segment 0xA000.
  ;
  ; arguments:
  ;   x : word -- x-coordinate
  ;   y : word -- y-coordinate
  ;   color : byte -- color from 256-color palette
  ;----------------------------------------------------------------
  set_pixel proc uses ax bx dx es, x:word, y:word, color:byte
      ; check if coordinates are not outside the screen
      cmp x, 0
      jl out_of_range ; x < 0
      cmp x, SCREEN_WIDTH
      jge out_of_range ; x >= SCREEN_WIDTH
      cmp y, 0
      jl out_of_range ; y < 0
      cmp y, SCREEN_HEIGHT
      jge out_of_range ; y >= SCREEN_HEIGHT

      ; write to the memory of VGA
      mov ax, 0a000h
      mov es, ax ; ES == 0xA000

      mov dx, SCREEN_WIDTH
      mov ax, y
      mul dx ; AX == SCREEN_WIDTH * y,  (DX modified)
      add ax, x ; AX == x + SCREEN_WIDTH * y
      mov bx, ax
      mov al, color
      mov byte ptr es:[bx], al ;  0xA800:[x+SCREEN_WIDTH*y] := color

    out_of_range:
      ret
  set_pixel endp


  ;----------------------------------------------------------------
  ; Converts decimal string to unsigned integer.
  ;
  ; arguments:
  ;   string : ptr -- pointer to the input string
  ;
  ; returns:
  ;   AX : word -- unsigned integer or (-1) on error
  ;----------------------------------------------------------------
  parse_unsigned proc uses bx dx, string:ptr byte
      ; AX -- result integer
      ; BX -- iterator over string

      xor ax, ax ; AX = 0
      mov bx, string

      ; iterate over string
    loop_begin:
      cmp byte ptr ds:[bx], 0
      je loop_end ; reached '\0'

      mov dx, 10
      mul dx ; result *= 10  (DX:AX = AX * DX)

      xor dh, dh
      mov dl, byte ptr ds:[bx] ; load current character to DX
      cmp dx, '0'
      jb invalid_character ; DX < '0'
      cmp dx, '9'
      ja invalid_character ; DX > '9'
      sub dx, '0'
      ; DX is in range 0-9

      add ax, dx ; add current digit to result

      inc bx ; move to next character
      jmp loop_begin
    loop_end:

      ; AX -- result
      jmp return

    invalid_character:
      mov ax, (-1)

    return:
      ret

  parse_unsigned endp


  ;----------------------------------------------------------------
  ; Performs a single interation of the L-system.
  ;
  ; Copies content of `l_system_buf1` into `l_system_buf2` replacing
  ; each 'F' with 'F-F++F-F', then copies content of
  ; `l_system_buf2` back to `l_system_buf1`.
  ;----------------------------------------------------------------
  lsystem_iteration proc uses ax bx si di
      ; SI -- iterator over `l_system_buf1`
      ; DI -- iterator over `l_system_buf2`
      ; BX -- iterator over `f_replacement`

      xor si, si
      xor di, di

      ; iterate over `l_system_buf1` using SI
    loop1_begin:
      mov al, byte ptr ds:[l_system_buf1+si]
      cmp al, 0
      je loop1_end ; l_system_buf1[SI] == '\0'

      cmp al, 'F'
      je case_f

      ; case '+' or '-'
      ; copy it to `l_system_buf2`
      mov byte ptr ds:[l_system_buf2+di], al
      inc di
      jmp switch_end

    case_f:
      ; case 'F'
      ; iterate over `f_replacement` using BX and copy it to `l_system_buf2`
      xor bx, bx
    inner_loop_begin:
      mov al, byte ptr ds:[f_replacement+bx]
      cmp al, 0
      je inner_loop_end

      mov byte ptr ds:[l_system_buf2+di], al
      inc bx
      inc di
      jmp inner_loop_begin
    inner_loop_end:

    switch_end:
      inc si
      jmp loop1_begin
    loop1_end:
      mov byte ptr ds:[l_system_buf2+di], 0 ; add null char at the end


      ; copy `l_system_buf2` back to `l_system_buf1`
      xor si, si
      xor di, di

    loop2_begin:
      mov al, byte ptr ds:[l_system_buf2+si]
      cmp al, 0
      je loop2_end

      mov byte ptr ds:[l_system_buf1+di], al
      inc si
      inc di
      jmp loop2_begin
    loop2_end:
      mov byte ptr ds:[l_system_buf1+di], 0

      ret
  lsystem_iteration endp


  ;----------------------------------------------------------------
  ; Performs `iterations` quantity of L-system iterations.
  ;----------------------------------------------------------------
  lsystem_all_iterations proc uses cx
      ; for CX in 0..iterations-1
      xor cx, cx
    loop_begin:
      cmp cx, iterations
      je loop_end

      invoke lsystem_iteration

      inc cx
      jmp loop_begin
    loop_end:

      ret
  lsystem_all_iterations endp


  ;----------------------------------------------------------------
  ; Performs a move of the virtual pen and draws a line.
  ;
  ; arguments:
  ;   ST1 -- start x-coordinate of the pen
  ;   ST0 -- start y-coordinate of the pen
  ;   dir : word -- one of the 6 supported directions,
  ;                 see global `sin` and `cos`
  ;   dist : word -- length of the line
  ;
  ; returns:
  ;   ST1 -- x-coordinate of the pen after the move
  ;   ST0 -- y-coordinate of the pen after the move
  ;----------------------------------------------------------------
  move proc uses cx si, dir:word, dist:word
      local x_int:word, y_int:word

      mov si, dir
      shl si, 3 ; SI == 8*dir

      ; for CX in 0..dist-1
      xor cx, cx
    loop_begin:
      cmp cx, dist
      je loop_end

      fadd qword ptr ds:[sin+si] ; y += sin[dir]
      fist y_int ; y_int = (int)y

      fxch st(1) ; ST0 <=> ST1
      fadd qword ptr ds:[cos+si] ; x += cos[dir]
      fist x_int ; x_int = int(x)

      fxch st(1) ; ST0 <=> ST1

      invoke set_pixel, x_int, y_int, 15

      inc cx
      jmp loop_begin
    loop_end:

      ret
  move endp


  ;----------------------------------------------------------------
  ; Draws Koch curve using data generated by
  ; L-system (`l_system_buf1`) by moving virtual
  ; pen (procedure `move`)
  ;----------------------------------------------------------------
  draw_koch_curve proc
      ; DX -- direction, 0..5
      ; begin with direction 5 -- angle 300 degrees
      mov dx, 5

      ; iterate over `l_system_buf1` using SI
      xor si, si
    loop_begin:
      mov al, byte ptr ds:[l_system_buf1+si]
      cmp al, 0
      je loop_end

      ; switch l_system_buf1[SI]
      cmp al, '+'
      je case_plus
      cmp al, '-'
      je case_minus

      ; case 'F'
      ; draw line of length `segment_length` towards direction DX
      invoke move, dx, segment_length
      jmp switch_end

    case_plus:
      ; case '+'
      ; rotate 60 degrees clockwise
      dec dx
      jmp switch_end

    case_minus:
      ; case '-'
      ; rotate 60 degrees counterclockwise
      inc dx

    switch_end:

      ; ensure DX is in range 0..5
      cmp dx, 0
      jl direction_lt_0
      cmp dx, 5
      jg direction_gt_5
      jmp direction_ok
    direction_lt_0:
      mov dx, 5
      jmp direction_ok
    direction_gt_5:
      mov dx, 0
    direction_ok:

      inc si
      jmp loop_begin
    loop_end:

      ret
  draw_koch_curve endp


  ;----------------------------------------------------------------
  ; Entry point of the program.
  ;----------------------------------------------------------------
  start proc
      ; initialize stack
      mov ax, _STACK
      mov ss, ax
      mov sp, offset stack_top ; SS:SP -- top of the stack

      ; initialize data segment
      mov ax, _DATA
      mov ds, ax ; DS == data segment

      invoke parse_args ; fills `args` and `arg_count`

      cmp arg_count, 2
      jne invalid_args ; `arg_count` != 2 -- invalid number of arguments

      ; 0th argument - number of iterations
      invoke get_arg, 0 ; AX - pointer to argument
      invoke parse_unsigned, ax ; AX -- integer or (-1) if error
      cmp ax, MIN_ITERATIONS
      jl invalid_args
      cmp ax, MAX_ITERATIONS
      jg invalid_args
      mov iterations, ax ; `iterations` == (int) 0th argument

      ; 1st argument - length of segments
      invoke get_arg, 1 ; AX - pointer to argument
      invoke parse_unsigned, ax ; AX -- integer or (-1) if error
      cmp ax, MIN_SEGMENT_LENGTH
      jl invalid_args
      cmp ax, MAX_SEGMENT_LENGTH
      jg invalid_args
      mov segment_length, ax ; `segment_length` == (int) 1st argument


      invoke lsystem_all_iterations

      ; remember current video mode
      invoke get_video_mode
      mov previous_video_mode, al

      ; change graphic mode to 320x200 pixels, 256 colors
      invoke set_video_mode, 13h

      finit

      ; set Rounding Control to Truncate
      fstcw fpu_cw
      or fpu_cw, 00000C00h ; set 10 & 11
      fldcw fpu_cw

      fild start_x
      fild start_y
      invoke draw_koch_curve

      ; wait for key press before terminating program
      invoke read_char

      ; restore previous video mode
      mov al, previous_video_mode
      invoke set_video_mode, al

      invoke exit, 0

    invalid_args:
      invoke print_cstring, addr str_invalid_args ; print error message
      invoke exit, 1 ; exit with non-zero code

  start endp

_CODE ends


_STACK segment stack
    dw 3ffh dup(?)
  stack_top:
    dw ?
_STACK ends


end start
