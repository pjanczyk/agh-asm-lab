;----------------------------------------------------------------
; (C) 2017  Piotr Janczyk
;
; Demo of parsing command-line arguments.
; MASM, 80186 processor, 16-bit mode, DOS
;----------------------------------------------------------------
.186
.model small, stdcall, nearstack

include libbase.asm ; procedures: `exit`, `print_char`, `print_cstring`

include libargs.asm ; procedures: `parse_args`
                    ; global variables: `args`, `arg_count`
	
_CODE segment

start proc
	mov ax, _STACK
	mov ss, ax ; SS == stack segment
	mov sp, offset stack_top

    mov ax, _DATA
    mov ds, ax ; DS == data segment
	
	call parse_args ; fills `args` and `arg_count`
	
	mov bx, offset args
	xor cx, cx
	mov cl, ds:[arg_count]
	
  loop_start:
    cmp cx, 0
    je finish
	invoke print_cstring, bx ; print argument, AX points to '\0'
	print_char 0Dh ; '\r'
	print_char 0Ah ; '\n'
	mov bx, ax
	inc bx ; BX points to next argument
	dec cx
	jmp loop_start
	
  finish:
	invoke exit, 0

start endp

_CODE ends

_STACK segment stack
	dw 3ffh dup(?)
  stack_top:
    dw ?
_STACK ends

end start
