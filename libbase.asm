_CODE segment

  ;----------------------------------------------------------------
  ; Prints single character to standard output.
  ;
  ; arguments:
  ;   byte -- ASCII character
  ;----------------------------------------------------------------
  print_char macro char:req
      push ax
      push dx

      ; Int 21/AH=02h - DOS 1+ - WRITE CHARACTER TO STANDARD OUTPUT
      ;   DL = character to write
      mov dl, char
      mov ah, 02h
      int 21h

      pop dx
      pop ax
  endm


  ;----------------------------------------------------------------
  ; Prints C-style string, i.e. string ended with '\0' character.
  ;
  ; arguments:
  ;   ptr byte -- pointer to the first character of the string
  ;
  ; returns:
  ;   AX : ptr byte -- pointer to '\0' character which ends the string
  ;----------------------------------------------------------------
  print_cstring proc uses bx, str_ptr:ptr byte
      mov bx, str_ptr
    loop_start:
      mov al, byte ptr [bx]
      cmp al, 0
      je loop_end
      print_char al
      inc bx
      jmp loop_start
    loop_end:
      mov ax, bx
      ret
  print_cstring endp


  ;----------------------------------------------------------------
  ; Checks whether two null-terminated strings are equal.
  ;
  ; arguments:
  ;   ptr byte -- pointer to the first character of the first string
  ;   ptr byte -- pointer to the first character of the second string
  ;
  ; returns:
  ;   AX -- 1, if the string are equal
  ;         0, otherwise
  ;----------------------------------------------------------------
  compare_cstrings proc uses si di es, str1:ptr byte, str2:ptr byte
      mov ax, ds
      mov es, ax ; ES == DS

      mov si, str1
      mov di, str2

    loop_start:
      mov al, byte ptr ds:[si]
      cmp al, byte ptr es:[di]
      jne not_equal ; [SI] != [DI] -- string are not equal
      cmp al, 0
      je equal
      inc si
      inc di
      jmp loop_start

    equal:
      ; [SI] == '\0' && [DI] == '\0'
      mov ax, 1 ; AX == 1 -- return true
      jmp return

    not_equal:
      mov ax, 0 ; AX == 0 -- return false

    return:
      ret

  compare_cstrings endp


  ;----------------------------------------------------------------
  ; Terminates program with exit code.
  ;
  ; arguments:
  ;   byte -- exit code
  ;----------------------------------------------------------------
  exit proc exit_code:byte
      ; Int 21/AH=4Ch - DOS 2+ - EXIT - TERMINATE WITH RETURN CODE
      ;   AL = return code
      mov ah, 4Ch
      mov al, byte
      int 21h
  exit endp

_CODE ends
