;----------------------------------------------------------------
; (C) 2017  Piotr Janczyk
;
; Encoding and decoding files using Vigenère cipher.
; MASM, 80186 processor, 16-bit mode, DOS
;----------------------------------------------------------------

.186
.model small, stdcall

include libbase.asm
include libargs.asm

BUFFOR_CAPACITY equ 16384 ; 16 KB


_DATA segment

  mode_decode db 0 ; 0 or 1
  password dw 0 ; pointer to the password (null-terminated string)
  input_file dw 0 ; handle to file
  output_file dw 0 ; handle to file

  input_buffor db BUFFOR_CAPACITY dup (?)
  input_buffor_pos dw 0 ; current position in input buffor, increased by `read_char`
  input_buffor_count dw 0 ; size of data in the buffor

  output_buffor db BUFFOR_CAPACITY dup (?)
  output_buffor_count dw 0 ; size of data in the buffor

  str_decode_flag db "-d", 0
  str_invalid_args db "Invalid arguments.", 13, 10, "Usage: vigenere.exe [-d] SOURCE DEST PASSWORD", 13, 10, 0
  str_file_open_error_1 db "Cannot open file ", '"', 0
  str_file_open_error_2 db '"', 13, 10, 0
  str_file_create_error_1 db "Cannot create file", '"', 0
  str_file_create_error_2 db '"', 13, 10, 0
  str_file_read_error db "An error occured while reading the file", 13, 10, 0
  str_file_write_error db "An error occured while writing to the file", 13, 10, 0

_DATA ends


_CODE segment

  ;----------------------------------------------------------------
  ; Opens a file.
  ; On success returns handle to the file, on error prints
  ; error message and terminates the program.
  ;
  ; arguments:
  ;   ptr -- path to the file (c-string)
  ;   byte -- access mode (r/w)
  ;
  ; returns:
  ;   AX : word -- file handle
  ;----------------------------------------------------------------
  file_open proc uses dx, path:ptr, mode:byte
      ; Int 21/AH=3Dh - DOS 2+ - OPEN - OPEN EXISTING FILE
      ;   AH = 3Dh
      ;   AL = access and sharing modes
      ;   DS:DX -> ASCIZ filename
      ;  Return:
      ;   CF clear if successful
      ;   AX = file handle
      ;   CF set on error
      ;   AX = error code (01h,02h,03h,04h,05h,0Ch,56h)
      mov ah, 3Dh
      mov al, mode
      mov dx, path
      int 21h

      jc err
      ; AX == file handle
      ret

    err:
      invoke print_cstring, offset str_file_open_error_1
      invoke print_cstring, path
      invoke print_cstring, offset str_file_open_error_2
      invoke exit, 1

  file_open endp


  ;----------------------------------------------------------------
  ; Creates a new file.
  ; On success returns handle to the file, on error prints
  ; error message and terminates the program.
  ;
  ; arguments:
  ;   ptr -- path of the file (c-string)
  ;
  ; returns:
  ;   AX : word -- file handle
  ;----------------------------------------------------------------
  file_create proc uses dx, path:ptr
      ; Int 21/AH=3Ch - DOS 2+ - CREATE - CREATE OR TRUNCATE FILE
      ;   AH = 3Ch
      ;   CX = file attributes
      ;   DS:DX -> ASCIZ filename
      ;  Return:
      ;   CF clear if successful
      ;   AX = file handle
      ;   CF set on error
      ;   AX = error code (03h,04h,05h)
      mov ah, 3Ch
      xor cx, cx
      mov dx, path
      int 21h

      jc err
      ; AX == file handle
      ret

    err:
      invoke print_cstring, offset str_file_create_error_1
      invoke print_cstring, path
      invoke print_cstring, offset str_file_create_error_2
      invoke exit, 1

  file_create endp


  ;----------------------------------------------------------------
  ; Closes an opened file.
  ;
  ; arguments:
  ;   word -- file handle
  ;----------------------------------------------------------------
  file_close proc uses ax bx, handle:word
      ; Int 21/AH=3Eh - DOS 2+ - CLOSE - CLOSE FILE
      ;   AH = 3Eh
      ;   BX = file handle
      ;  Return:
      ;   CF clear if successful
      ;   AX destroyed
      ;   CF set on error
      ;   AX = error code (06h - invalid handle)
      mov ah, 3Eh
      mov bx, handle
      int 21h
      ret
  file_close endp

  ;----------------------------------------------------------------
  ; Buffered reading `input_file` (global variable).
  ;
  ; returns:
  ;   CF -- 0 if success;
  ;         1 if reached end-of-file
  ;   AL: byte -- next byte from the file
  ;----------------------------------------------------------------
  read_char proc uses bx dx
      mov dx, input_buffor_count
      cmp dx, input_buffor_pos
      jne read_from_buffor

      ; Int 21/AH=3Fh - DOS 2+ - READ - READ FROM FILE OR DEVICE
      ;   AH = 3Fh
      ;   BX = file handle
      ;   CX = number of bytes to read
      ;   DS:DX -> buffer for data
      ;  Return:
      ;   CF clear if successful
      ;   AX = number of bytes actually read (0 if at EOF before call)
      ;   CF set on error
      ;   AX = error code (05h,06h)

      mov ah, 3Fh
      mov bx, input_file
      mov cx, BUFFOR_CAPACITY
      mov dx, offset input_buffor
      int 21h

      jc err

      mov input_buffor_pos, 0
      mov input_buffor_count, ax
      cmp ax, 0
      jne read_from_buffor

      ; end of file
      stc
      jmp return

    read_from_buffor:
      mov bx, input_buffor_pos
      mov al, byte ptr ds:[input_buffor+bx]
      inc input_buffor_pos
      clc

    return:
      ret

    err:
      invoke print_cstring, offset str_file_read_error
      invoke exit, 1

  read_char endp


  ;----------------------------------------------------------------
  ; Empties output buffor and writes its content to
  ; file `output_file` (global variable).
  ;----------------------------------------------------------------
  flush proc uses ax bx cx dx
      ; Int 21/AH=40h - DOS 2+ - WRITE - WRITE TO FILE OR DEVICE
      ;   AH = 40h
      ;   BX = file handle
      ;   CX = number of bytes to write
      ;   DS:DX -> data to write
      ;  Return:
      ;   CF clear if successful
      ;   AX = number of bytes actually written
      ;   CF set on error
      ;   AX = error code (05h,06h)

      mov ah, 40h
      mov bx, output_file
      mov cx, output_buffor_count
      mov dx, offset output_buffor
      int 21h

      jc err

      mov output_buffor_count, 0

      ret

    err:
      invoke print_cstring, offset str_file_read_error
      invoke exit, 1

  flush endp


  ;----------------------------------------------------------------
  ; Buffered writing to `output_file` (global variable)
  ;
  ; arguments:
  ;   byte - char to be written
  ;----------------------------------------------------------------
  write_char proc uses ax bx dx, char:byte
      cmp output_buffor_count, BUFFOR_CAPACITY
      jne write_to_buffor

      invoke flush

    write_to_buffor:
      mov al, char
      mov bx, output_buffor_count
      mov byte ptr ds:[output_buffor+bx], al
      inc output_buffor_count

    return:
      ret

  write_char endp


  ;----------------------------------------------------------------
  ; Reads data from input file,
  ; decodes/encodes (according to `mode_decode`) using `password`
  ; and writes the result to output file.
  ;----------------------------------------------------------------
  encode proc uses ax bx si
      ; BX -- start of the password
      ; SI -- index of the current element in password
      mov bx, password
      xor si, si ; SI == 0

    loop_begin:
      cmp byte ptr ds:[bx+si], 0
      jne handle_char
      ; reached the end of the password, start over
      xor si, si
      jmp loop_begin

    handle_char:
      invoke read_char ; AL == next char
                       ; if end-of-file, CF == 1
      jc eof

      cmp mode_decode, 0
      jnz handle_decode

      ; encoding mode
      ; output_char := (input_char + password_char) mod 256
      add al, byte ptr ds:[bx+si]
      jmp handle_write

      ; decoding mode
      ; output_char := (input_char - password_char) mod 256
    handle_decode:
      sub al, byte ptr ds:[bx+si]

    handle_write:
      print_char al
      invoke write_char, al

      inc si
      jmp loop_begin

    eof:
      invoke flush
      ret

  encode endp


  ;----------------------------------------------------------------
  ; Entry point of the program.
  ;----------------------------------------------------------------
  start proc
      mov ax, _STACK
      mov ss, ax
      mov sp, offset stack_top ; SS:SP -- top of the stack

      mov ax, _DATA
      mov ds, ax ; DS == data segment

      invoke parse_args ; fills `args` and `arg_count`

      cmp byte ptr ds:[arg_count], 3
      je handle_paths ; arg_count == 3

      cmp byte ptr ds:[arg_count], 4
      jne invalid_args ; arg_count != 4 && arg_count != 3 -- invalid number of arguments

      ; arg_count == 4
      invoke get_arg, 0 ; AX - pointer to 0th argument
      invoke compare_cstrings, ax, offset str_decode_flag ; AX == 1, if 0th argument is equal "-d"
      cmp ax, 0
      jz invalid_args
      mov mode_decode, 1

    handle_paths:
      xor dl, dl
      add dl, mode_decode

      invoke get_arg, dl ; AX == DL'th argument
      invoke file_open, ax, 0 ; 0 - read only mode
      mov input_file, ax ; `input_file` == handle to input file

      inc dl

      invoke get_arg, dl ; AX == DL'th argument
      invoke file_create, ax
      mov output_file, ax ; `output_file` == handle to output file

      inc dl

      invoke get_arg, dl
      mov password, ax ; `password` == DL'th argument

      invoke encode

      invoke file_close, input_file
      invoke file_close, output_file

      invoke exit, 0

    invalid_args:
      invoke print_cstring, addr str_invalid_args ; print error message
      invoke exit, 1 ; exit with non-zero code

  start endp

_CODE ends


_STACK segment stack
    dw 3ffh dup(?)
  stack_top:
    dw ?
_STACK ends


end start
